# Wedding-Planner

## Project Description

The Wedding Planner application is an online tool for a wedding company. It is used to add, remove, and edit wedding information in a database. There is an application service to verify login information of users. The application also features a messaging service so employees can communicate with each other. 


## Technologies Used

* Node.js - version 14
* Compute Engine
* App Engine
* Cloud Functions
* Cloud SQL
* Cloud Storage
* Cloud Datastore
* Firebase Hosting

## Features
* Authentication Service for user login and messaging
* Messaging Service for employee communication
* Wedding Planner API for adding, editing, or removing data
* Image upload service for attaching photos to expenses

## Getting started

* In git bash run `git clone https://gitlab.com/mitchell.kirk/wedding-planner.git`
* In Windows, open the weddingplannerreact folder.
* In Windows, open command prompt and run `npm i` to install all dependencies
* In the command prompt, run `npm start`. This will host the frontend locally.

## Usage
Login Page
* To login use the email "mitchell.kirk@revature.net" and password "pa55word".

![Login Page](./images/login.png)

Home Page
* Use the options on the home page to navigate between the messaging center and wedding planner pages.

![Home Page](./images/home.png)

Wedding Planner Page
* From here you can view, edit, add, and delete weddings and expenses. For creating new expenses there is also a file upload service so you can attach a photo of any expenses.

![Wedding Planner Page](./images/wedding_planner.png)

Compose Message Page
* From here you can compose and send messages to other employees. The other employee in the database is Adam@email.com

![Compose Message Page](./images/compose_message.png)

Search Messages Page
* From here you can search messages by either sender, recipient, or both!

![Search Messages Page](./images/search_messages.png)

